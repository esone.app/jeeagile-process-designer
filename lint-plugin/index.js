module.exports = {
  configs: {
    recommended: {
      rules: {
        'target-namespace': 'error'
      }
    },
    all: {
      rules: {
        'target-namespace': 'warn',
        'no-manual-task': 'warn',
        // bpmnlint 默认校验组件（方便后续修改）
        'conditional-flows': 'error',
        'end-event-required': 'error',
        'event-sub-process-typed-start-event': 'error',
        'fake-join': 'warn',
        'label-required': 'error',
        'no-complex-gateway': 'error',
        'no-disconnected': 'error',
        'no-duplicate-sequence-flows': 'error',
        'no-gateway-join-fork': 'error',
        'no-implicit-split': 'error',
        'no-inclusive-gateway': 'error',
        'single-blank-start-event': 'error',
        'single-event-definition': 'error',
        'start-event-required': 'error',
        'sub-process-blank-start-event': 'error',
        'superfluous-gateway': 'warning'
      }
    }
  }
}
