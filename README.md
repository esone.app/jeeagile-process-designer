# jeeagile-process-designer

基于BPMN、VUE、ELEMENT-UI定制的个性化在线流程编辑器


**在线体验地址：**  [http://process.jeeagile.vip](http://process.jeeagile.vip)

## 更新日志
*  2022-01-06
    * 优化自定义属性配置支持自定义实现
*  2021-12-31
    * 自定义节点属性设置完成
*  2021-11-29
	* 基于BPMN、VUE、ELEMENT-UI定制的个性化在线流程编辑器
	
## 使用说明
### 使用方法
```vue
<template>
  <div id="app">
    <process-designer
      v-model="processXml"
      :process-id="processId"
      :process-name="processName"
      :process-desc="processDesc"
      :process-xml="processXml"
      :process-prefix="processPrefix"
      :process-properties="processProperties"/>
  </div>
</template>

<script>
  import ProcessDesigner from './components/process-designer'
  import CustomProcessProperties from './custom/CustomProperties'

  export default {
    name: 'App',
    components: {
      ProcessDesigner
    },
    data() {
      return {
         processId: `process_${new Date().getTime()}`,
         processName: `流程_${new Date().getTime()}`,
         processPrefix: 'activiti',
         processDesc: '',
         processXml: null,
         processProperties: CustomProcessProperties
      }
    }
  }
</script>
```
process-prefix工作流类型目前支持activiti、camunda、flowable

process-properties自定义节点属性配置分组，已内置属性分组（可覆盖）

### 属性配置模版
```vue
<template>
  <div class="properties-item__content">
    <el-form v-model="templateInfo" :inline="false" label-width="100px" size="small" label-position="right">
      <el-form-item label="模板:">
        <el-input v-model="templateInfo.id" @change="updateBaseInfo('id')"/>
      </el-form-item>
    </el-form>
  </div>
</template>

<script>
  import processHelper from '../../helper/ProcessHelper'

  export default {
    name: 'Template',
    props: {
      processModeler: {  // 流程Modeler
        type: Object,
        required: true
      },
      processInfo: {    // 流程基础信息
        type: Object,
        required: true
      },
      activeElement: {  // 当前选中元素
        type: Object,
        required: true
      }
    },
    data() {
      return {
        templateInfo: {}
      }
    },
    watch: {
      activeElement: {
        handler() {
          if (this.activeElement) {
            let businessObject = this.activeElement.businessObject
            this.templateInfo = JSON.parse(JSON.stringify(businessObject))
          }
        },
        deep: true,
        immediate: true
      }
    },
    methods: {
      updateBaseInfo(key) {
        const properties = Object.create(null)
        properties[key] = this.templateInfo[key]
        processHelper.updateProperties(this.activeElement, properties)
      }
    }
  }
</script>
<style lang='scss'>
</style>
```
已内置节点属性配置项，可根据name进行覆盖

| name                    | title                | sort                |
| ----------------------- | -------------------  | ------------------- | 
| base                    | 基本设置               | 0                   |
| process                 | 流程设置               | 1                   |
| userTask                | 用户设置               | 2                   |
| receiveTask             | 接收设置               | 3                   |
| sendTask                | 发送设置               | 4                   |
| scriptTask              | 脚本设置               | 5                   |
| form                    | 表单设置               | 6                   |
| instance                | 会签配置               | 7                   |
| async                   | 持续异步               | 8                   |
| condition               | 条件流转               | 9                   |
| taskListeners           | 任务监听               | 10                  |
| executeListeners        | 执行监听               | 11                  |
| button                  | 按钮配置               | 12                  |
| input                   | 输入参数               | 13                  |
| output                  | 输出参数               | 14                  |
| extensions              | 扩展属性               | 15                  |
| documentation           | 元素文档               | 16                  |

属性分组是根据节点类型进行分组配置，将根据节点类型获取对应的属性配置分组，如果name相同自定义的将覆盖默认配置
```js
import UserTask from './properties/UserTask'

/** 用户任务 */
export const UserTaskProperties = {
  name: 'userTask',
  title: '用户设置',
  icon: 'el-icon-user',
  sort: 2,
  component: UserTask
}


/** 用户任务 属性分组 */
export const UserTaskPropertiesGroup = [
  UserTaskProperties
]

export default {
  /** 用户任务 */
  'bpmn:UserTask': UserTaskPropertiesGroup
}
```
### 安装依赖
```
npm install
```

### 启动服务
```
npm run serve
```

### 编译发布
```
npm run build
```

